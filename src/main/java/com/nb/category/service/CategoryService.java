﻿package com.nb.category.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.dao.sql.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nb.article.model.Article;
import com.nb.category.model.Category;
import com.nb.cms.model.Site;
import com.nb.common.service.BaseService;
import com.nb.system.NewPager;


/**
 * 文章管理实现类
 * @author 赵占涛 369880281@qq.com
 *
 */
@Service
public class CategoryService  extends BaseService{

	@Autowired
	private Dao dao;
	

	//得到当前站点的所有栏目
	public List<Category> getAll() {
		Site site = dao.fetch(Site.class, Cnd.where("isdefault","=","1"));
		return dao.query(Category.class, Cnd.where("siteid","=",site.getId()));
	}

	public int delete(int id) {
		return dao.delete(Category.class, id);
	}

	public Category fetch(int id) {
		return dao.fetch(Category.class, id);
	}

	public Map<String, Object> queryPage(NewPager page) {
		page.setOrderBy("id");
		page.setOrder("desc");
		Criteria cri = getCriteriaFromPage(page);
		
	    List<Category> list = dao.query(Category.class, cri, page);
	    page.setRecordCount(dao.count(Category.class, cri));
	    
	    Map<String,Object> map = new HashMap<String,Object>();
		map.put("Total", page.getRecordCount());
		map.put("Rows", list);
	    return map;
	}

	public Category insert(Category en) {
		return dao.insert(en);
	}

	public void updateIgnoreNull(Category en) {
		dao.updateIgnoreNull(en);
	}
	
}
