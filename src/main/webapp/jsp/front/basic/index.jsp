<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>

<%@ include file="header.jsp"%>

	<script type="text/javascript" >

	 $(document).ready(function() {
		 //图片轮播
        //$('#coin-slider').coinslider({ width: 990,navigation: true, delay: 5000,effect:"random",links : false  });
		 
		 $('#scroll_3 .a_bigImg img').sGallery({
			 thumbObj:'#scroll_3 .ul_scroll_a2 span',
			 thumbNowClass:'on',//自定义导航对象当前class为on
			 changeTime:4000//自定义切换时间为4000ms
			 });
    });
	</script>
<!--main-->
	<div class="banner center" >
	<div class="scrollBox_a1" id="scroll_3">
<a href="#1" class="a_bigImg"><img src="${img_path}/slider/01.jpg" width="980" height="250" alt="" /></a>
<a href="#2" class="a_bigImg"><img src="${img_path}/slider/02.jpg" width="980" height="250" alt="" /></a>
<a href="#3" class="a_bigImg"><img src="${img_path}/slider/03.jpg" width="980" height="250" alt="" /></a>
<a href="#4" class="a_bigImg"><img src="${img_path}/slider/04.jpg" width="980" height="250" alt="" /></a>
<ul class="ul_scroll_a2">
<li><span>1</span></li>
<li><span>2</span></li>
<li><span>3</span></li>
<li><span>4</span></li>
</ul>
</div>
	</div>
	<div class="content center">
		<div class="news left">
			<span class="more"><a href="{$CATEGORYS[8]['url']}">more</a></span>
			<ul>
			<c:forEach items="${cms:getArticles(4,10)}" var="it" >
				<li><a href="{$v['url']}" title="{$v['title']}" target="_blank">${it.title}</a></li>
			</c:forEach>
			</ul>
		</div>
		<div class="about left">
			<span class="more"><a href="{$CATEGORYS[6]['url']}">more</a></span>
			<img src="${img_path}/about_img.jpg"/>
			<p>
			<c:forEach items="${cms:getArticles(2,1)}" var="it" >
				${fn:substring(cms:getArticleData(it.id).content,0,98)}
			<a href="${ctx}/aa/${it.id}.html" >[更多]</a></p>
			</c:forEach>
		</div>
		<div class="contact left">
			<span class="tel">(+86)18622041848</span>
			<p>加入我们的qq群，一起分享与交流:375595237</p>
			<p class="add"> 点击下载最新版本代码 下载</p>
			<p>代码获取：<a href="http://git.oschina.net/mefly/niceboat" target="_bank" >http://git.oschina.net/mefly/niceboat</a></p>
			<ul class="online">
				<li class="left"><img src="${img_path}/code.png" width="66" height="66" /><li>
				<li class="code left">手机扫描左侧二维码快速获取联系方式<li>
			</ul>
		</div>
	</div>
</div>