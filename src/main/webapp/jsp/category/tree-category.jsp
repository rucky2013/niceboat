<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>
<script>

var setting = {
		view: {
			addHoverDom: addHoverDom,
			removeHoverDom: removeHoverDom,
			selectedMulti: false
		},
		edit:{
			enable: true,
			showRemoveBtn: false,
			showRenameBtn: false
		},
		check: {
			enable: false//是否显示checkbox
		},
		async: {
			enable: true,
			url:"${ctx}/admin/category/ajax_tree"
		},
		data: {
			key:{
				name:"name"
			},
			simpleData: {
				enable: true,//true时下面的设置生效
				idKey: "id",//id
				pIdKey: "pid",//pid
			}
		},
		callback: {
			onAsyncSuccess: zTreeOnAsyncSuccess//异步加载成功
		}
	};
//异步加载成功后    展开所有节点
function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
	treeObj.expandAll(true);
};
var newDialog;
// 焦点	
function addHoverDom(treeId, treeNode) {
	var sObj = $("#" + treeNode.tId + "_span");
	if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
	var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
		+ "' title='add node' onfocus='this.blur();'></span>";
	var editStr = "<span class='button edit' id='editBtn_" + treeNode.tId
		+ "' title='edit node' onfocus='this.blur();'></span>";
	sObj.after(addStr+editStr);
	var addBtn = $("#addBtn_"+treeNode.tId);
	if (addBtn) addBtn.bind("click", function(){
		$("#inputPage").load('${ctx}/admin/category/input?pid='+treeNode.id);
	});
	var editBtn = $("#editBtn_"+treeNode.tId);
	if (editBtn) editBtn.bind("click", function(){
		$("#inputPage").load('${ctx}/admin/category/input?id='+treeNode.id);
	});
};
//移开焦点	
function removeHoverDom(treeId, treeNode) {
	$("#addBtn_"+treeNode.tId).unbind().remove();
	$("#editBtn_"+treeNode.tId).unbind().remove();
};

var treeObj = null;
$(function ()
{	
	treeObj = $.fn.zTree.init($("#menuTree"), setting);

	$("#backGrid").click(function(){
		   $(".rig").load("${ctx}/jsp/system/list-department.jsp");			
	});
});
</script>
<style type="text/css">
.ztree li span.button.add {margin-left:2px; margin-right: -1px; background-position:-144px 0; vertical-align:top; *vertical-align:middle}
ul.ztree {
    background: none repeat scroll 0 0 #f0f6e4;
    border: 1px solid #d5d5d5;
    height: 360px;
    margin-top: 10px;
    overflow-x: auto;
    overflow-y: scroll;
    width: 490px;
}
</style>
</head>
<body style="background-color: #fff">
	<ul id="menuTree" class="ztree" style="float: left;overflow-x:hidden ;overflow-y:auto;height: 350px;width:400px;"></ul>
	<div id="inputPage" style="float: left; width: 600px;margin:10px">
</div>
</body>	
</html>

